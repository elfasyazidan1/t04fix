import java.util.InputMismatchException;
import java.util.Scanner;

import javax.xml.transform.Source;

public class ExceptionHandling {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.println("Program Pembagian Bilangan");

        boolean validInput = false;
        do {
            try {
                System.out.println("pembilang : ");
                int a = scanner.nextInt();
                System.out.println("penyebut  : ");
                int b = scanner.nextInt();
                System.out.println(pembagian(a, b));
                validInput = true;
            } catch (ArithmeticException e) {
                System.out.println("something went wrong");
            } catch (InputMismatchException e){
                System.out.println("something went wrong");
                scanner.nextLine();
            }
            //edit here
        } while (!validInput);
        scanner.close();
    }
    public static int pembagian(int pembilang, int penyebut) {
        //add exception apabila penyebut bernilai 0
        if(penyebut == 0) { 
            throw new ArithmeticException("tidak boleh angka 0");
        }
        return pembilang / penyebut;
    }
}
